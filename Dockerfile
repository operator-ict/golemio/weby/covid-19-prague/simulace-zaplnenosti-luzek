FROM bitnami/python:3.8
WORKDIR /app

COPY requirements.txt .
RUN pip install -r requirements.txt
COPY ./*.py ./

# Create a non-root user
RUN useradd -r -u 1001 -g root nonroot && \
    chown -R nonroot /app
USER nonroot

CMD ["bokeh", "serve", "app.py", "--allow-websocket-origin=localhost"]
