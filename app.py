from prediction import predict_aro_jip_capacity, load_luzka_capacity, \
    get_luzka_for_prediction, load_uzis_data, get_share_hospitalized, \
    derive_exponent, get_daily_positives_start, get_uzis_for_prediction

# attach to VS Code debugger if this script was run with BOKEH_VS_DEBUG=true
import os
if 'BOKEH_VS_DEBUG' in os.environ:
    if os.environ['BOKEH_VS_DEBUG'] == 'true':
        import ptvsd
        # 5678 is the default attach port in the VS Code debug configurations
        print('Waiting for debugger attach')
        ptvsd.enable_attach(address=('localhost', 5678), redirect_output=True)
        ptvsd.wait_for_attach()

from functools import partial

from bokeh.plotting import figure, curdoc
from bokeh.models import ColumnDataSource, Slider, Div, BoxAnnotation, \
    Paragraph, Span, Label, Toggle, TableColumn, DataTable
from bokeh.layouts import column, layout
from math import pi
import pandas as pd
from sqlalchemy import create_engine
from datetime import datetime, timedelta
from dotenv import load_dotenv

load_dotenv('env.env')

GEOMEAN_DAYS = 15
DEFAULT_SHARE_CRITICAL = 0.25
DEFAULT_HOSPITALIZATION_DELAY = 10
COLORS = ['#3cba54', '#f4c20d', '#db3236', '#4885ed']

TEXT_CAPACITY_FULL = '''
    <h5>ARO/JIP</h5>
    obsazeno: {arojip_yesterday_full} (odhad) <br />
    volných lůžek C+: {arojip_cplus_available} (dle DIP) <br />
    naplní se: <strong>{arojip_cplus_full_at}</strong> <br />
    volných lůžek C+ a C-: {arojip_cplus_cminus_available} (dle DIP) <br />
    naplní se: <strong>{arojip_cplus_cminus_full_at}</strong> <br />
    <h5>Lůžka s kyslíkem</h5>
    obsazeno: {stdoxy_yesterday_full} (odhad) <br />
    volných lůžek C+: {stdoxy_cplus_available} (dle DIP) <br />
    naplní se: <strong>{stdoxy_cplus_full_at}</strong> <br />
    volných lůžek C+ a C-: {stdoxy_cplus_cminus_available} (dle DIP) <br />
    naplní se: <strong>{stdoxy_cplus_cminus_full_at}</strong> <br />
    '''

EXPLANATIONS = '''
<div>
    <h3 style="text-align: center;">Manuál</h3>
    <p><strong>Co to je? </strong></p>
    <p><strong>POZOR: Simulace zaplněnosti lůžek byla vytvořena pro použití v
    průběhu růstové fáze epidemie. Výsledky příliš nedávají smysl během
    stagnace či poklesu epidemie.</strong></p>
    <p>Simulace, která na základě dat o vývoji epidemie v Praze a
    ve Středočeském kraji odhaduje počet Covid+ pacientů vyžadujících
    hospitalizaci a srovnává jí s aktuálními daty o kapacitě nemocnic udávanými
    Dispečinkem Intenzivní Péče MZČR (DIP). </p>
    <p>Odhad je rozdělený na dvě části: V 1. části odhadujeme na datech o
    reálném počtu nakažených počet pacientů vyžadujících hospitalizaci.
    Ve 2. části pak na základě předpokladu exponenciálního růstu
    (definovaného <i>Exponentem růstu pozitivních</i>) simulujeme budoucí
    počet nakažených a z něho teprve odvozujeme počet hospitalizovaných.</p>
    <p>Budeme velmi rádi za Vaši zpětnou vazbu! Pošlete nám prosím email na
    <a href="mailto:golemio@operatorict.cz">golemio@operatorict.cz</a>.</p>
    <p><strong>Jak číst graf? </strong></p>
        <ul>
            <li><i>Vodorovné přerušované (-.-.) čáry</i> představují součet
            současné volné kapacity nemocnic dle DIP s odhadem počtu v
            současnosti hospitalizovaných pacientů v dané
            kategorii (ARO/JIP a Standardní lůžka s kyslíkem).
            Odhad je vypočítán dle <i>Podílu ARO/JIP na všech
            hospitalizovaných</i> na posledním dostupném
            (tj. včerejším) počtu hospitalizovaných osob.
            Čára níže představuje lůžka vyhrazená pro Covid+ pacienty a vyšší
            čára je představuje součet lůžek pro Covid+ a Covid-
            (viz vstupní data).</li>
            <li><i>Plné čáry</i> představují skutečný vývoj - v tomto případě
            tedy jde pouze o <i>přírůstek pozitivních</i>
            a <i>počet hospitalizací</i>.</li>
            <li><i>Tečkované (...) čáry</i> jsou simulované výsledky.</li>
        </ul>
     <p>Pro pochopení grafu je nutné mezi sebou srovnávat pouze čáry, které
     spolu souvisí - jsou barevně sjednoceny.
     Tedy zejména současnou kapacitu lůžek dle DIP a odhad jejich vývoje.
     Kdy se tyto čáry protnou (tj. počet simulovaných potřebných lůžek
     převýší udávané kapacity lůžek, které jsou k dispozici)
     je indikováno v boxu vpravo nahoře.</p>

    <p>Graf je rozdělen na tři oblasti: </p>
        <ul>
            <li><span style="background-color:white">Historie</span></li>
            <li><span style="background-color:rgba(0,0,255,0.1)">Blízká
            budoucnost</span>, která vychází ze skutečných dat o počtu
            pozitivních</li>
            <li><span style="background-color:rgba(255,192,203,0.25)">
            Simulovaná budoucnost</span>, která vychází ze simulace počtu
            pozitivních.</li>
        </ul>

    <p><strong>Postup odhadu: </strong></p>
    <ol>
        <li><i><span style="color:{1}">Přírůstek pozitivních</span></i>:
        Na poslední dostupný bod v časové řadě (v grafu
        <i>Přírůstek pozitivních (skutečnost)</i>), je navázán odhad budoucího
        vývoje (<i>Přírůstek pozitivních (odhad)</i>)
        Budoucí vývoj je definován dvěma parametry:
        <i>Exponent růstu pozitivních</i> a <i>Dnešní nárůst pozitivních</i>
        </li>
        <li><i>Přírůstek hospitalizací</i>: Počet nově hospitalizovaných je
        vypočítán jako <i>Podíl hospitalizovaných z pozitivních</i> zpožděný o
        <i>Počet dní mezi pozitivitou a hospitalizací</i></li>
        <li><i><span style="color:{0}">Počet hospitalizovaných</span></i>:
        Součet přírůstku hositalizací za posledních X dní (parametr <i>Počet
        dní v nemocncici</i>.
        Všimněte si, že odhadovaný počet hospitalizací velmi dobře
        koresponduje s těmi skutečnými. Vzroste-li počet pozitivních,
        o X dní později vzroste počet hospitalizovaných.</li>
        <li>
            <i>
            <span style="color:{2}">Počet hospitalizovaných na ARO/JIP
            </span>
        </i> a
        <i>
            <span style="color:{3}">Standardních lůžkách s kyslíkem
            </span>
        </i>:
        Počet hospitalizovaných je rozdělen mezi ARO/JIP a
        Standardní lůžka s kyslíkem v poměru definovaném v parametru
        <i>Podíl ARO/JIP na všech hospitalizovaných</i></li>
    </ol>
    <p><strong>Vstupní data: </strong></p>
    <p>Zdrojem veškerých dat je ÚZIS a jeho informační systém DIP.
    Použity jsou následující datové sady:
        <ul>
            <li>Denní přírůstek nových pozitivních</li>
            <li>Počet aktuálně hospitalizovaných pacientů v nemocnicích</li>
            <li>Aktuální počet volných lůžek v nemocnicích v kategoriích
            <i>ARO/JIP</i> a <i>Standardní lůžka s kyslíkem</i>.
            Rozlišuje se mezi lůžky vyhrazenými pro Covid+ a Covid- pacienty,
            nicméně v případě problémů budou Covid+ pacienty použity všechny
            volné kapacity.</li>
        </ul>
        <p>Všechny datové sady jsou k dispozici v agregaci pouze pro Prahu
        a pro Středočeský kraj.</p>
</div>
'''.format(*COLORS)

HEADER = '''
<div>
    <a href="https://golemio.cz/cs/oblasti" target="_blank">
        <img
            src="https://golemio.cz/themes/golem/assets/brand/GolemioGrey.svg"
            width="180"
            style="position:absolute; top:15px;left:10px;" />
        </a>
    <h3 style="text-align: center;">Simulace zaplněnosti lůžek v Praze
    a ve Středočeském kraji</h3>
    <a href="https://operatorict.cz/" target="_blank">
        <img
            src="https://golemio.cz/themes/golem/assets/brand/OICT_grey.svg"
            width="55"
            style="position:absolute; top:15px;right:10px;"
        />
    </a>
</div>
'''

EXPLANATION_STYLE = {
    'background-color': '#F8F8F8',
    'width': '100%',
    'padding': '20px',
    'margin': '10x',
}
db = create_engine(os.getenv('DB_CONNSTRING'))


def collect_parameters(df, geomean_days, share_critical, hospitalization_delay):
    return {
        'CASE_LENGTH_DAYS': 12,
        'SHARE_CRITICAL': share_critical,
        'SHARE_HOSPITALIZED': get_share_hospitalized(df, hospitalization_delay),
        'HOSPITALIZATION_DELAY': hospitalization_delay,
        'EXPONENT': derive_exponent(df, geomean_days),
        'DAILY_POSITIVES_START': get_daily_positives_start(df),
    }


def create_dashboard_layout(controls, figure, capacity_div, capacity_table):
    expl = Div(text=EXPLANATIONS, style=EXPLANATION_STYLE)
    header = Div(text=HEADER, style=EXPLANATION_STYLE)
    return layout(header, [controls, figure, [capacity_div, capacity_table]], expl)


def get_current_data(db):
    uzis = load_uzis_data(db)
    luzka = load_luzka_capacity(db)
    return uzis, luzka


def draw_figure(source, yesterday, capacity, d):
    fig = figure(
        x_axis_label='',
        y_axis_label='Počet případů',
        x_axis_type='datetime',
        plot_height=500,
        plot_width=500,
        y_range=(1, 4000)
    )

    fig.yaxis.formatter.use_scientific = True

    fig.xaxis.formatter.days = '%d/%m/%Y'
    fig.xaxis.major_label_orientation = pi / 4

    fig.add_layout(
        BoxAnnotation(
            left=datetime.today() - timedelta(days=1),
            right=datetime.today() + timedelta(days=d['HOSPITALIZATION_DELAY'] - 1),
            fill_alpha=0.1,
            fill_color='blue',
            name='area_RealData'
        )
    )

    fig.add_layout(
        BoxAnnotation(
            left=datetime.today() + timedelta(days=d['HOSPITALIZATION_DELAY'] - 1),
            fill_alpha=0.1,
            fill_color='pink',
            name='area_SimulatedData'
        )
    )

    fig.add_layout(
        Span(
            location=capacity['AROJIP_BEDS_AVAILABLE_COVID'] + yesterday.predict_critical,
            dimension='width',
            line_color=COLORS[2],
            line_dash='dashdot',
            line_width=1
        )
    )

    fig.add_layout(
        Label(
            x=pd.to_datetime('2020-08-01'),
            y=capacity['AROJIP_BEDS_AVAILABLE_COVID'] + yesterday.predict_critical,
            text='ARO/JIP k dispozici C+',
            text_font_size='10px',
            y_offset=3,
            text_color=COLORS[2]
        )
    )

    fig.add_layout(
        Span(
            location=capacity['AROJIP_BEDS_AVAILABLE_NECOVID'] + capacity['AROJIP_BEDS_AVAILABLE_COVID'] + yesterday.predict_critical,
            dimension='width',
            line_color=COLORS[2],
            line_dash='dashdot',
            line_width=1
        )
    )

    fig.add_layout(
        Label(
            x=pd.to_datetime('2020-08-01'),
            y=capacity['AROJIP_BEDS_AVAILABLE_NECOVID'] + capacity['AROJIP_BEDS_AVAILABLE_COVID'] + yesterday.predict_critical,
            text='ARO/JIP k dispozici C+ a C-',
            text_font_size='10px',
            y_offset=3,
            text_color=COLORS[2]
        )
    )

    fig.add_layout(
        Span(location=capacity['STANDARD_OXYGEN_BEDS_AVAILABLE_COVID'] + (yesterday.real_hospitalized - yesterday.predict_critical),
             dimension='width',
             line_color=COLORS[3],
             line_dash='dashdot',
             line_width=1
             )
    )

    fig.add_layout(
        Label(
            x=pd.to_datetime('2020-08-01'),
            y=capacity['STANDARD_OXYGEN_BEDS_AVAILABLE_COVID'] + (yesterday.real_hospitalized - yesterday.predict_critical),
            text='Std Oxy k dispozici C+',
            text_font_size='10px',
            y_offset=3,
            text_color=COLORS[3]
        )
    )

    fig.add_layout(
        Span(
            location=capacity['STANDARD_OXYGEN_BEDS_AVAILABLE_NECOVID'] + capacity['STANDARD_OXYGEN_BEDS_AVAILABLE_COVID'] +
            (yesterday.real_hospitalized - yesterday.predict_critical),
            dimension='width',
            line_color=COLORS[3],
            line_dash='dashdot',
            line_width=1
        )
    )

    fig.add_layout(
        Label(
            x=pd.to_datetime('2020-08-01'),
            y=capacity['STANDARD_OXYGEN_BEDS_AVAILABLE_NECOVID'] + capacity['STANDARD_OXYGEN_BEDS_AVAILABLE_COVID'] +
            (yesterday.real_hospitalized - yesterday.predict_critical),
            text='Std Oxy k dispozici C+ a C-',
            text_font_size='10px',
            y_offset=3,
            text_color=COLORS[3]
        )
    )

    fig.line('date', 'real_incidence',
             legend_label="Přírůstek pozitivních (skutečnost)",
             line_width=1,
             source=source,
             line_color=COLORS[1]
             )

    fig.line('date', 'predict_incidence',
             legend_label="Přírůstek pozitivních (odhad)",
             line_width=1,
             source=source,
             line_dash='dotted',
             line_color=COLORS[1]
             )

    fig.line('date', 'real_hospitalized',
             legend_label="Hospitalizace (skutečnost)",
             line_width=1,
             source=source,
             line_color=COLORS[0]
             )

    fig.line('date', 'predict_hospitalized',
             legend_label="Hospitalizace (odhad)",
             line_width=1,
             source=source,
             line_dash='dotted',
             line_color=COLORS[0]
             )

    fig.line('date', 'predict_std_beds',
             legend_label="Std. lůžka s kyslíkem (odhad)",
             line_width=2,
             source=source,
             line_dash='dotted',
             line_color=COLORS[3]
             )

    fig.line('date', 'predict_critical',
             legend_label="Kritické případy ARO/JIP (odhad)",
             line_width=2,
             source=source,
             line_dash='dotted',
             line_color=COLORS[2]
             )

    fig.legend.location = "top_left"

    return fig


def get_controls(d, source, yesterday, df, date_capacity_div, doc, includeSTC, fig):
    desc_style = {
        'font-style': 'italic',
        'font-size': 'smaller'
    }

    controls = {}
    controls['select_region_toggle'] = Toggle(label='započítat Středočeský kraj', active=includeSTC)

    controls['exp_slider'] = Slider(
        start=1,
        end=2,
        value=d['EXPONENT'],
        step=.01,
        title='Exponent růstu pozitivních'
    )

    controls['exp_desc'] = Paragraph(
        text=f'odhad růstu v posledních {GEOMEAN_DAYS} dnech',
        style=desc_style
    )

    controls['share_hospit_slider'] = Slider(
        start=0.01,
        end=0.6,
        value=d['SHARE_HOSPITALIZED'],
        step=.01,
        title="Podíl hospitalizovaných z pozitivních")

    controls['share_hosp_desc'] = Paragraph(
        text='počet hospitalizovaných/počet infekčních před {HOSPITALIZATION_DELAY} dny'.format(**d),
        style=desc_style)

    controls['level_slider'] = Slider(
        start=100,
        end=round(df.incidence.max(), -3) + 1000,
        value=d['DAILY_POSITIVES_START'],
        step=10,
        title="Dnešní nárůst pozitivních")

    controls['level_desc'] = Paragraph(
        text='Přírůstek pozitivních {}'.format((datetime.today() - timedelta(days=1)).strftime('%d. %m. %Y')),
        style=desc_style)

    controls['share_crit_slider'] = Slider(
        start=0.01,
        end=0.75,
        value=d['SHARE_CRITICAL'],
        step=0.01,
        title="Podíl ARO/JIP na všech hospitalizovaných")

    controls['hosp_delay_slider'] = Slider(
        start=1,
        end=30,
        value=d['HOSPITALIZATION_DELAY'],
        step=1,
        title="Počet dní mezi pozitivitou a hospitalizací")

    controls['case_length_slider'] = Slider(start=1, end=30, value=d['CASE_LENGTH_DAYS'], step=1, title="Počet dní v nemocnici")

    callback_partial = partial(
        callback,
        d=d,
        df=df,
        controls=controls,
        source=source,
        yesterday=yesterday,
        date_capacity_div=date_capacity_div,
        fig=fig
        )
    reload_partial = partial(reload_dashboard, doc=doc)

    controls['select_region_toggle'].on_change('active', reload_partial)
    controls['exp_slider'].on_change('value', callback_partial)
    controls['share_hospit_slider'].on_change('value', callback_partial)
    controls['level_slider'].on_change('value', callback_partial)
    controls['share_crit_slider'].on_change('value', callback_partial)
    controls['hosp_delay_slider'].on_change('value', callback_partial)
    controls['case_length_slider'].on_change('value', callback_partial)

    return controls


def get_capacity_info(prediction, d, yesterday):
    return {
        'arojip_yesterday_full': round(yesterday.predict_critical),
        'arojip_cplus_available': d['AROJIP_BEDS_AVAILABLE_COVID'],
        'arojip_cplus_full_at': get_full_capacity_date(
            prediction, 'predict_critical',
            d['AROJIP_BEDS_AVAILABLE_COVID'] + yesterday.predict_critical
            ),
        'arojip_cplus_cminus_available': d['AROJIP_BEDS_AVAILABLE_NECOVID'] + d['AROJIP_BEDS_AVAILABLE_COVID'],
        'arojip_cplus_cminus_full_at': get_full_capacity_date(
            prediction,
            'predict_critical',
            d['AROJIP_BEDS_AVAILABLE_COVID'] + d['AROJIP_BEDS_AVAILABLE_NECOVID'] + yesterday.predict_critical),
        'stdoxy_yesterday_full': round(yesterday.real_hospitalized - yesterday.predict_critical),
        'stdoxy_cplus_available': d['STANDARD_OXYGEN_BEDS_AVAILABLE_COVID'],
        'stdoxy_cplus_full_at': get_full_capacity_date(
            prediction,
            'predict_std_beds',
            d['STANDARD_OXYGEN_BEDS_AVAILABLE_COVID'] + (yesterday.real_hospitalized - yesterday.predict_critical)
            ),
        'stdoxy_cplus_cminus_available': d['STANDARD_OXYGEN_BEDS_AVAILABLE_NECOVID'] + d['STANDARD_OXYGEN_BEDS_AVAILABLE_COVID'],
        'stdoxy_cplus_cminus_full_at': get_full_capacity_date(
            prediction,
            'predict_std_beds',
            d['STANDARD_OXYGEN_BEDS_AVAILABLE_NECOVID'] + d['STANDARD_OXYGEN_BEDS_AVAILABLE_COVID'] +
            (yesterday.real_hospitalized - yesterday.predict_critical)),
    }


def get_full_capacity_date(prediction, pred_series, threshold, dateformat='%d. %m. %Y'):
    ser = prediction[(prediction[pred_series] >= threshold) & (prediction.date > pd.to_datetime(datetime.today().date(), utc=True))].date
    if ser.shape[0] > 0:
        return ser.iloc[0].strftime(dateformat)
    else:
        return 'NA'


def get_capacity_div(prediction, d, yesterday):

    capacity_info = get_capacity_info(prediction, d, yesterday)

    return Div(text=TEXT_CAPACITY_FULL.format(**capacity_info),
               background="#F8F8F8",
               style={
                   'text-align': 'center',
                   'width': '100%',
                   'padding': '10px',
                   'vertical-align': 'middle'}
               )


def get_capacity_table(luzka):
    renames = {
        'aro_jip_total': 'Celkem ARO/JIP',
        'aro_jip_cplus': 'C+ volná ARO/JIP',
        'aro_jip_cminus': 'C- volná ARO/JIP',
        'stdoxy_total': 'Celkem lůžka s kys.',
        'stdoxy_cplus': 'C+ volná lůžka s kys.',
        'stdoxy_cminus': 'C- volná lůžka s kys.'}

    table = luzka.T
    table['category'] = table.index.map(renames)
    table = table[table.category.apply(lambda x: 'Celkem' not in x)]
    capacity_source = ColumnDataSource(table)
    columns = [
        TableColumn(field="category", title="", width=150),
        TableColumn(field="PHA", title="Praha", width=75),
        TableColumn(field="STC", title="Středočeský kraj", width=75),
    ]
    return column(
        Div(
            text='Aktuálně volná lůžka:',
            style={'font-weight': 'bold'}
        ),
        DataTable(
            source=capacity_source,
            columns=columns,
            index_position=None,
            width=300,
            height=150
        )
    )


def update_prediction(df, d, source, yesterday, date_capacity_div, fig):
    prediction = predict_aro_jip_capacity(db, df, d)

    source.stream(prediction, rollover=prediction.shape[0])

    capacity_info = get_capacity_info(prediction, d, yesterday)

    date_capacity_div.text = TEXT_CAPACITY_FULL.format(**capacity_info)

    shift_annotations(fig, d)


def shift_annotations(fig, d):
    boxes = [ref for ref in fig.references() if ref.struct['type'] == 'BoxAnnotation']
    simdata = [box for box in boxes if box.name == 'area_SimulatedData'][0]
    realdata = [box for box in boxes if box.name == 'area_RealData'][0]
    realdata.right = (datetime.today() + timedelta(days=d['HOSPITALIZATION_DELAY'] - 1)).timestamp() * 1000
    simdata.left = (datetime.today() + timedelta(days=d['HOSPITALIZATION_DELAY'] - 1)).timestamp() * 1000


def callback(attr, old, new, d, df, controls, source, yesterday, date_capacity_div, fig):
    d['EXPONENT'] = controls['exp_slider'].value
    d['SHARE_HOSPITALIZED'] = controls['share_hospit_slider'].value
    d['DAILY_POSITIVES_START'] = controls['level_slider'].value
    d['SHARE_CRITICAL'] = controls['share_crit_slider'].value
    d['HOSPITALIZATION_DELAY'] = controls['hosp_delay_slider'].value
    d['CASE_LENGTH_DAYS'] = controls['case_length_slider'].value

    update_prediction(df, d, source, yesterday, date_capacity_div, fig)


def get_yesterday(prediction):
    return prediction[prediction.date < pd.to_datetime(datetime.today(), utc=True)].iloc[-2]


def init_layout(doc, includeSTC):
    # Download data from database
    uzis, luzka = get_current_data(db)

    capacity = get_luzka_for_prediction(luzka, includeSTC)

    df = get_uzis_for_prediction(uzis, includeSTC)

    d = collect_parameters(df, GEOMEAN_DAYS, DEFAULT_SHARE_CRITICAL, DEFAULT_HOSPITALIZATION_DELAY)

    d = {**d, **capacity}

    prediction = predict_aro_jip_capacity(db, df, d)

    source = ColumnDataSource(prediction)

    yesterday = get_yesterday(prediction)

    fig = draw_figure(source, yesterday, capacity, d)

    date_capacity_div = get_capacity_div(prediction, d, yesterday)
    capacity_table = get_capacity_table(luzka)

    controls = get_controls(d, source, yesterday, df, date_capacity_div, doc, includeSTC, fig)

    return create_dashboard_layout([controls[ui] for ui in controls], fig, date_capacity_div, capacity_table)


def reload_dashboard(attr, old, new, doc):
    if new != old:
        includeSTC = new
        doc.clear()
        dashboard = init_layout(doc, includeSTC)
        doc.add_root(dashboard)


doc = curdoc()
dashboard = init_layout(doc, includeSTC=True)
doc.add_root(dashboard)
doc.title = 'Simulace zaplněnosti lůžek v Praze - Datová platforma Golemio'
