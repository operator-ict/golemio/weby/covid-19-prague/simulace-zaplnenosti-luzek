import pandas as pd
from scipy.stats import gmean
from datetime import datetime, timedelta


def load_uzis_data(db):
    df = pd.read_sql_query('''
    select * from uzis.epid_kraje ek
    ''', db)
    df.Datum = pd.to_datetime(df.Datum, utc=True)
    return df


def get_uzis_for_prediction(df, includeSTC):
    dfcopy = df.copy()
    if includeSTC:
        regs = ['CZ010', 'CZ020']
    else:
        regs = ['CZ010']
    dfcopy = dfcopy[dfcopy.Kraj.isin(regs)]
    result = dfcopy.groupby('Datum').sum().sort_index(ascending=True)
    result.tz_localize(None)
    result.index = result.index.normalize()
    return result[:-1]


def load_luzka_capacity(db):
    return pd.read_sql_query('''
    select
        region,
        sum("ARO+JIP_luz_C+") as aro_jip_cplus,
        sum("ARO+JIP_luz_C-") as aro_jip_cminus,
        sum("ARO+JIP_luz_celkem") as aro_jip_total,
        sum("Standard_luz_s_kys_C+") as stdoxy_cplus,
        sum("Standard_luz_s_kys_C-") as stdoxy_cminus,
        sum("Standard_luz_s_kys_celkem") as stdoxy_total
    from uzis.dip_beds_current dbc
    where region in ('PHA','STC')
    group by region
    ''', db, index_col='region')


def get_luzka_for_prediction(luzka, includeSTC):
    if includeSTC:
        ser = luzka.sum(axis=0)
    else:
        ser = luzka.loc['PHA']
    return {
        'AROJIP_BEDS_TOTAL': int(ser.aro_jip_total),
        'AROJIP_BEDS_AVAILABLE_COVID': int(ser.aro_jip_cplus),
        'AROJIP_BEDS_AVAILABLE_NECOVID': int(ser.aro_jip_cminus),
        'STANDARD_OXYGEN_BEDS_TOTAL': int(ser.stdoxy_total),
        'STANDARD_OXYGEN_BEDS_AVAILABLE_COVID': int(ser.stdoxy_cplus),
        'STANDARD_OXYGEN_BEDS_AVAILABLE_NECOVID': int(ser.stdoxy_cminus)
    }


def get_daily_positives_start(uzis):
    return uzis.incidence.iloc[-1]


def get_last_hospitalized(uzis):
    return uzis.aktualni_pocet_hospitalizovanych_osob.iloc[-1]


def derive_exponent(uzis, geomean_days):
    inc = uzis.incidence.dropna()
    return max(gmean((inc / inc.shift(1)).iloc[-geomean_days:]), 1)


def get_share_hospitalized(uzis, delay):
    return (uzis.aktualni_pocet_hospitalizovanych_osob / uzis.prevalence.shift(delay)).iloc[-1]


def predict_aro_jip_capacity(db, uzis, d):
    history = uzis.incidence.copy().loc['2020-08-01':]
    today = pd.to_datetime(datetime.today().date(), utc=True)
    prediction = pd.Series(
        {today + timedelta(days=i - 1): d['DAILY_POSITIVES_START'] * (d['EXPONENT']**i) for i in range(1, 28)}
    ).rename('incidence')
    prediction = pd.concat([history, prediction]).to_frame()
    prediction.index = pd.to_datetime(prediction.index, utc=True)

    prediction['predict_new_hospitalized'] = prediction.incidence.shift(d['HOSPITALIZATION_DELAY']) * d['SHARE_HOSPITALIZED']
    prediction['predict_hospitalized'] = prediction.predict_new_hospitalized.rolling(d['CASE_LENGTH_DAYS']).sum()
    prediction['real_hospitalized'] = uzis.aktualni_pocet_hospitalizovanych_osob
    prediction['predict_critical'] = prediction.predict_hospitalized * d['SHARE_CRITICAL']
    prediction['predict_std_beds'] = prediction.predict_hospitalized - prediction.predict_critical

    prediction = prediction.reset_index().rename({'index': 'date'}, axis=1)

    prediction['real_incidence'] = prediction[prediction.date < today].incidence
    prediction['predict_incidence'] = prediction[prediction.date >= today].incidence
    return prediction
