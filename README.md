# Interaktivní dasboard pro jednoduchý prediktivní model pro lůžkovou kapacitu v Praze

Cíl: Predikovat lůžkovou kapacitu v Praze

Dosažený výsledek: Jednoduchý model má k predikci prozatím dost daleko, ale implementuje navždy rostoucí exponenciálu. Po určitou dobu může fungovat. 

Autor: Vítek Macháček; machacek.vit@operatorict.cz

Datum: 23.9.2020

Analyzované období: 1. 8. 2020 - konec roku

## Předpoklady
Po určitou dobu, která je pro nás relevantní poroste počet nakažených dle exponenciální funkce 
## Vstup
Model automaticky fituje některé parametry na data ÚZIS, která se na rabínovi (tabulka `uzis.uzis_covid19_prague_districts`) ukládá.

Hlavně: 
1. Exponent růstu za posledních 15 dní (geometrický průměr růstu nakažených v posledních 15 dnech)
2. poslední nárůst pozitivních (počet pozitivních včera)
3. Podíl hospitalizovaných na celkovém počtu pozitivních před 8 dny.

## Výstup
Interaktivní dashboard - viz adresa! TBD

## Spuštění
1. Vytvořit soubor `env.env` s odkazem na rabína dle `env.env-example`
2. spustit `Dockerfile` či v CLI v samostatném virtualenvu spustit: `pip install -r requirements.txt` a následně `bokeh serve bokeh.py`

## Metodologie

viz funkce `predict_aro_jip_capacity` v souboru `prediction.py`

1. Z dat o incidenci (tj. počtu denních nárůstů pozitivních) protáhnout exponenciálu dle parametru `EXPONENT`

2. Odhadnout prevalenci (tj. počet aktuálně pozitivních) z dat o incidenci

3. Z incidence odhadnout počet aktuálně hospitalizovaných pomocí parametru `HOSPITALIZATION_DELAY`

4. Z aktuálně hospitalizovaných odhadnout počet případů pro ARO/JIP pomocí parametru `SHARE_CRITICAL`


## Problémy k řešení
přidat do luzkoveho dashboardu:
	- možnost postupně navyšovat či snižovat určitý počet lůžek - slider o kolik denně/týdně + dodat historii kapacity lůžek v čase
	- pravidelny reload dat v databazi?? Jednou za 60 minut treba? Obecná otázka, jak by stahování mělo probíhat.


